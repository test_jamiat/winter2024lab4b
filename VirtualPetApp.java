import java.util.Scanner;

public class VirtualPetApp {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        Dog[] kennel = new Dog[4];

        for (int i = 0; i < kennel.length; i++) {

            System.out.println("Enter the name of the dog");
            String name = reader.nextLine();

            System.out.println("Enter the age of the dog");
            int age = reader.nextInt();

            reader.nextLine();

            System.out.println("Enter the breed of the dog");
            String breed = reader.nextLine();
			
            kennel[i] = new Dog(name, age, breed);
        }
		System.out.println("Input the new dog's age");
		int age = reader.nextInt();
		
		System.out.println("Your dog's name is " + kennel[kennel.length-1].getName());
        System.out.println(kennel[kennel.length-1].getName() + "'s human age is " + kennel[kennel.length-1].getAge());
        System.out.println(kennel[kennel.length-1].getName() + "'s breed is " + kennel[kennel.length-1].getBreed());
		
		kennel[kennel.length-1].setAge(age);
		
		System.out.println("Your dog's name is " + kennel[kennel.length-1].getName());
        System.out.println(kennel[kennel.length-1].getName() + "'s human age is " + kennel[kennel.length-1].getAge());
        System.out.println(kennel[kennel.length-1].getName() + "'s breed is " + kennel[kennel.length-1].getBreed());
    }
}	